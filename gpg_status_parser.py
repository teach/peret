import re

class GoodSignature:

    def __init__(self, long_keyid_or_fpr, *username):
        self.long_keyid_or_fpr = long_keyid_or_fpr
        self.username = " ".join(username)


class ExpiredSignature:

    def __init__(self, long_keyid_or_fpr, *username):
        self.long_keyid_or_fpr = long_keyid_or_fpr
        self.username = " ".join(username)


class ExpiredKey:

    def __init__(self, long_keyid_or_fpr, *username):
        self.long_keyid_or_fpr = long_keyid_or_fpr
        self.username = " ".join(username)


class RevokedKey:

    def __init__(self, long_keyid_or_fpr, *username):
        self.long_keyid_or_fpr = long_keyid_or_fpr
        self.username = " ".join(username)


class BadSignature:

    def __init__(self, long_keyid_or_fpr, *username):
        self.long_keyid_or_fpr = long_keyid_or_fpr
        self.username = " ".join(username)


class UncheckableSignature:

    def __init__(self, keyid, pkalgo, hashalgo, sig_class, time, rc, longkeyid):
        self.keyid = keyid
        self.pkalgo = pkalgo
        self.hashalgo = hashalgo
        self.sig_class = sig_class
        self.time = time
        self.rc = rc
        self.longkeyid = longkeyid


class ValidSignature:

    def __init__(self, fingerprint_in_hex, sig_creation_date, sig_timestamp, expire_timestamp, sig_version, reserved, pubkey_algo, hash_algo, sig_class, primary_key_fpr=None):
        self.fingerprint_in_hex = fingerprint_in_hex
        self.sig_creation_date = sig_creation_date
        self.sig_timestamp = sig_timestamp
        self.expire_timestamp = expire_timestamp
        self.sig_version = sig_version
        self.reserved = reserved
        self.pubkey_algo = pubkey_algo
        self.hash_algo = hash_algo
        self.sig_class = sig_class
        self.primary_key_fpr = primary_key_fpr


class SignatureID:

    def __init__(self, radix64_string, sig_creation_date, sig_timestamp):
        self.radix64_string = radix64_string
        self.sig_creation_date = sig_creation_date
        self.sig_timestamp = sig_timestamp


class EncryptedTo:

    def __init__(self, long_keyid, keytype, keylength):
        self.long_keyid = long_keyid
        self.keytype = keytype
        self.keylength = keylength


class NoPublicKey:

    def __init__(self, long_keyid):
        self.long_keyid = long_keyid


class _Trust:

    def __init__(self, flag, validation_model=None):
        self.flag = flag
        self.validation_model = validation_model

class TrustUndefined(_Trust):

    def __str__(self):
        return "unknown"

class TrustNever(_Trust):

    def __str__(self):
        return "never"

class TrustMarginal(_Trust):

    def __str__(self):
        return "marginal"

class TrustFully(_Trust):

    def __str__(self):
        return "fully"

class TrustUltimate(_Trust):

    def __str__(self):
        return "ultimate"


_keywords = {
    "GOODSIG": GoodSignature,
    "EXPSIG": ExpiredSignature,
    "EXPKEYSIG": ExpiredKey,
    "REVKEYSIG": RevokedKey,
    "BADSIG": BadSignature,
    "ERRSIG": UncheckableSignature,
    "VALIDSIG": ValidSignature,
    "SIG_ID": SignatureID,
    "ENC_TO": EncryptedTo,
    "NO_PUBKEY": NoPublicKey,

    "TRUST_UNDEFINED": TrustUndefined,
    "TRUST_NEVER": TrustNever,
    "TRUST_MARGINAL": TrustMarginal,
    "TRUST_FULLY": TrustFully,
    "TRUST_ULTIMATE": TrustUltimate,
}

def parse(fd):
    line = fd.readline()
    context = {}
    while line:
        res = re.match(r"^\[GNUPG:\] (?P<keyword>\S+)(?: (?P<args>.*))?$", line.decode('utf-8', 'replace'))
        if res is not None:
            keyword = res.group("keyword")
            args = res.group("args").split(" ") if res.group("args") is not None else []

            if keyword == "NEWSIG":
                if len(context) > 0:
                    yield context
                context = {}

            elif keyword in _keywords:
                if keyword[:5] == "TRUST":
                    context["TRUST"] = _keywords[keyword](*args)
                context[keyword] = _keywords[keyword](*args)
        line = fd.readline()

    yield context
