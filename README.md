Peret
=====

Peret checks PGP signed mails and extract submissions contained in it.


Usage
-----

A basic usage is through procmail; append thoses lines into your
`~/.procmailrc`:

```
:0H
* ^Delivered-To: peret@domain\.tld
{
    :0c
    * !^X-loop:
    | /path/to/peret/check.py --real-send --submissions /path/to/shemu/submissions/TP3 --soft-max-submission "october 20 08:42" --hard-max-submission "october 20 09:21"

    :0
    $DEFAULT
}
```
