import subprocess

from test import MailTest

def run_script(dest, script):
    with subprocess.Popen(
            ["/bin/sh", script],
            cwd=dest,
            env={"LANG": 'C'},
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
    ) as p:
        p.wait()
        err = p.stdout.read().decode('utf-8', 'replace')
        err += p.stderr.read().decode('utf-8', 'replace')
        if p.returncode == 0:
            yield MailTest("Preliminary tests:", details=err.replace(dest, "/rendu/"))
            yield dest
        elif p.returncode == 124 or p.returncode == 137:
            yield MailTest("Preliminary tests aborted for too long execution:", 1, details=err.replace(dest, "/rendu/"))
        else:
            yield MailTest("An error occured during preliminary tests:", 2, details=err.replace(dest, "/rendu/"))
            yield dest
