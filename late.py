from datetime import datetime, timezone

from test import MailTest


def check(cnt, hard_max_submission_date, soft_max_submission_date=None):
    if soft_max_submission_date is None:
        soft_max_submission_date = hard_max_submission_date
    else:
        soft_max_submission_date = min(soft_max_submission_date, hard_max_submission_date)

    now = datetime.now().replace(tzinfo=timezone.utc)
    yield MailTest("We are %s, submission permitted until %s" % (now.strftime("%c"), soft_max_submission_date.strftime("%c")), -1)
    if now > soft_max_submission_date:
        discard = now > hard_max_submission_date
        yield MailTest("Submissions have been closed since %s minute(s)" % int((now - soft_max_submission_date).total_seconds()/60), 1 if discard else 2)
        if discard:
            return

    yield cnt
