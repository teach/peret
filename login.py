import csv
from email.utils import parseaddr

from test import MailTest

LASTNAME_FIELD = 0
FIRSTNAME_FIELD = 1
LOGIN_FIELD = 2
EMAILADDR_FIELD = 3


def check(cnt, *files):
    data, uname = cnt
    username, address = parseaddr(uname)

    for file in files:
        with open(file, encoding='utf-8') as fd:
            people = csv.reader(fd)
            for p in people:
                if address.lower() == p[EMAILADDR_FIELD].lower() or uname.lower().find(p[LOGIN_FIELD].lower()) >= 0 or username.lower().replace(" ", "").find(p[LASTNAME_FIELD].lower().replace(" ", "")) >= 0 and username.lower().find(p[FIRSTNAME_FIELD].lower()) >= 0:
                    yield MailTest("Recognized as %s: %s %s." % (p[LOGIN_FIELD], p[FIRSTNAME_FIELD], p[LASTNAME_FIELD]))
                    yield data, p[LOGIN_FIELD]
                    return

    yield MailTest("The username of your key is not explicit, I can't find you.", 1)
