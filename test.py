class MailTest:

    def __init__(self, title, value=0, details=None):
        self._title = title
        self._value = value
        self._details = details

    @property
    def title(self):
        return self._title

    @property
    def value(self):
        return self._value

    @property
    def valuestr(self):
        if self._value < -1:    return "       "
        elif self._value < 0:  return "[    ] "
        elif self._value == 0: return "[ OK ] "
        elif self._value == 2: return "[WARN] "
        elif self._value == 1: return "[FAIL] "

    @property
    def details(self):
        return self._details
